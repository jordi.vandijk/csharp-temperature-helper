﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UdemyTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Temperature temperature = new(TemperatureUnits.Kelvin, 50);
            Temperature temperature1 = new(TemperatureUnits.Celsius, 1);
            Temperature temperature2 = new(TemperatureUnits.Fahrenheit, 225.15);
            Temperature temperature3 = new(TemperatureUnits.Celsius, 3);

            List<Temperature> temps = new();
            temps.Add(temperature);
            temps.Add(temperature1);
            temps.Add(temperature2);
            temps.Add(temperature3);

            Console.WriteLine("The lowest temperature = " + temps.Min().ToString());
            Console.WriteLine("The highest temperature = " + temps.Max().ToString());

            if (temperature1 == temperature2)
            {
                Console.WriteLine($"{temperature1} is the same temperature as {temperature2}");
            }
            else if (temperature1 > temperature2)
            {
                Console.WriteLine($"{temperature1} is hotter than {temperature2}");
            }
            else
            {
                Console.WriteLine($"{temperature1} is colder than {temperature2}");
            }

            Console.WriteLine($"{temperature1} + {temperature2} =");
            Console.WriteLine(temperature1 + temperature2);

            Console.WriteLine("Now some sorting, list before sort:");
            foreach (Temperature temp in temps)
            {
                Console.WriteLine(temp);
            }
            Console.WriteLine("Now the sorted list");
            temps.Sort();
            foreach (Temperature temp in temps)
            {
                Console.WriteLine(temp);
            }

            Console.ReadLine();
        }
    }
}
