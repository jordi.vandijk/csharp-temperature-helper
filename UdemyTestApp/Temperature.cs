﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdemyTestApp
{
    public enum TemperatureUnits
    {
        Celsius,
        Fahrenheit,
        Kelvin
    }

    public class Temperature : IComparable<Temperature>, IEquatable<Temperature>, IEqualityComparer<Temperature>
    {
        public TemperatureUnits Unit { get; private set; }
        public double Value { get; set; }

        public Temperature(TemperatureUnits unit, double value)
        {
            this.Value = value;
            this.Unit = unit;
        }

        #region conversions

        private readonly Func<double, double> CelToFar = d => Math.Round((d * 9) / 5 + 32, 2);
        private readonly Func<double, double> FarToCel = d => Math.Round((d - 32) * 5 / 9, 2);
        private readonly Func<double, double> CelToKel = d => Math.Round(d + 273.15, 2);
        private readonly Func<double, double> KelToCel = d => Math.Round(d - 273.15, 2);
        private readonly Func<double, double> KelToFar = d => Math.Round((d - 273.15) * 1.8 + 32, 2);
        private readonly Func<double, double> FarToKel = d => Math.Round((d - 32) / 1.8 + 273.15, 2);

        public void ToKelvin()
        {
            switch (Unit)
            {
                case TemperatureUnits.Celsius:
                    Value = CelToKel(Value);
                    Unit = TemperatureUnits.Kelvin;
                    break;
                case TemperatureUnits.Fahrenheit:
                    Value = FarToKel(Value);
                    Unit = TemperatureUnits.Kelvin;
                    break;
                default:
                case TemperatureUnits.Kelvin:
                    break;
            }
        }

        public void ToCelsius()
        {
            switch (Unit)
            {
                default:
                case TemperatureUnits.Celsius:
                    break;
                case TemperatureUnits.Fahrenheit:
                    Value = FarToCel(Value);
                    Unit = TemperatureUnits.Celsius;
                    break;
                case TemperatureUnits.Kelvin:
                    Value = KelToCel(Value);
                    Unit = TemperatureUnits.Celsius;
                    break;
            }
        }

        public void ToFahrenheit()
        {
            switch (Unit)
            {
                case TemperatureUnits.Celsius:
                    Value = CelToFar(Value);
                    Unit = TemperatureUnits.Fahrenheit;
                    break;
                default:
                case TemperatureUnits.Fahrenheit:
                    break;
                case TemperatureUnits.Kelvin:
                    Value = KelToFar(Value);
                    Unit = TemperatureUnits.Fahrenheit;
                    break;
            }
        }

        public override string ToString()
        {
            return Unit switch
            {
                TemperatureUnits.Celsius => $"{Value} \u00B0C",
                TemperatureUnits.Fahrenheit => $"{Value} \u00B0F",
                TemperatureUnits.Kelvin => $"{Value} \u00B0K",
                _ => $"{Value}",
            };
        }
        #endregion

        #region compareable, equatable, equalityComparer
        public bool Equals(Temperature other)
        {
            if (other != null)
            {
                return Unit switch
                {
                    TemperatureUnits.Celsius => other.Unit switch
                    {
                        TemperatureUnits.Celsius => Value.Equals(other.Value),
                        TemperatureUnits.Fahrenheit => Value.Equals(FarToCel(other.Value)),
                        TemperatureUnits.Kelvin => Value.Equals(KelToCel(other.Value)),
                        _ => false
                    },
                    TemperatureUnits.Fahrenheit => other.Unit switch
                    {
                        TemperatureUnits.Celsius => Value.Equals(CelToFar(other.Value)),
                        TemperatureUnits.Fahrenheit => Value.Equals(other.Value),
                        TemperatureUnits.Kelvin => Value.Equals(KelToFar(other.Value)),
                        _ => false
                    },
                    TemperatureUnits.Kelvin => other.Unit switch
                    {
                        TemperatureUnits.Celsius => Value.Equals(CelToKel(other.Value)),
                        TemperatureUnits.Fahrenheit => Value.Equals(FarToKel(other.Value)),
                        TemperatureUnits.Kelvin => Value.Equals(other.Value),
                        _ => false
                    },
                    _ => false,
                };
            }
            else
            {
                return false;
            }
        }

        public int CompareTo(Temperature other)
        {
            if (other != null)
            {
                return Unit switch
                {
                    TemperatureUnits.Celsius => other.Unit switch
                    {
                        TemperatureUnits.Celsius => Value.CompareTo(other.Value),
                        TemperatureUnits.Fahrenheit => Value.CompareTo(FarToCel(other.Value)),
                        TemperatureUnits.Kelvin => Value.CompareTo(KelToCel(other.Value)),
                        _ => 1
                    },
                    TemperatureUnits.Fahrenheit => other.Unit switch
                    {
                        TemperatureUnits.Celsius => Value.CompareTo(CelToFar(other.Value)),
                        TemperatureUnits.Fahrenheit => Value.CompareTo(other.Value),
                        TemperatureUnits.Kelvin => Value.CompareTo(KelToFar(other.Value)),
                        _ => 1
                    },
                    TemperatureUnits.Kelvin => other.Unit switch
                    {
                        TemperatureUnits.Celsius => Value.CompareTo(CelToKel(other.Value)),
                        TemperatureUnits.Fahrenheit => Value.CompareTo(FarToKel(other.Value)),
                        TemperatureUnits.Kelvin => Value.CompareTo(other.Value),
                        _ => 1
                    },
                    _ => 1,
                };
            }
            else
            {
                return 1;
            }
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode() * 17 + this.Unit.GetHashCode();
        }

        public bool Equals(Temperature x, Temperature y)
        {
            return x == y;
        }

        public int GetHashCode([DisallowNull] Temperature obj)
        {
            return obj.Value.GetHashCode() * 17 + obj.Unit.GetHashCode();
        }
        #endregion

        #region operators
        public static bool operator ==(Temperature left, Temperature right)
        {
            if (ReferenceEquals(left, null))
            {
                return ReferenceEquals(right, null);
            }

            return left.Equals(right);
        }

        public static bool operator !=(Temperature left, Temperature right)
        {
            return !(left == right);
        }

        public static bool operator <(Temperature left, Temperature right)
        {
            return ReferenceEquals(left, null) ? !ReferenceEquals(right, null) : left.CompareTo(right) < 0;
        }

        public static bool operator <=(Temperature left, Temperature right)
        {
            return ReferenceEquals(left, null) || left.CompareTo(right) <= 0;
        }

        public static bool operator >(Temperature left, Temperature right)
        {
            return !ReferenceEquals(left, null) && left.CompareTo(right) > 0;
        }

        public static bool operator >=(Temperature left, Temperature right)
        {
            return ReferenceEquals(left, null) ? ReferenceEquals(right, null) : left.CompareTo(right) >= 0;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (ReferenceEquals(obj, null))
            {
                return false;
            }

            throw new NotImplementedException();
        }

        public static Temperature operator +(Temperature left, Temperature right)
        {
            double amount = left.Unit switch
            {
                TemperatureUnits.Celsius => right.Unit switch
                {
                    TemperatureUnits.Celsius => left.Value + right.Value,
                    TemperatureUnits.Fahrenheit => left.Value + right.FarToCel(right.Value),
                    TemperatureUnits.Kelvin => left.Value + right.KelToCel(right.Value),
                    _ => left.Value
                },
                TemperatureUnits.Fahrenheit => right.Unit switch
                {
                    TemperatureUnits.Celsius => left.Value + right.CelToFar(right.Value),
                    TemperatureUnits.Fahrenheit => left.Value + right.Value,
                    TemperatureUnits.Kelvin => left.Value + right.KelToFar(right.Value),
                    _ => left.Value
                },
                TemperatureUnits.Kelvin => right.Unit switch
                {
                    TemperatureUnits.Celsius => left.Value + right.CelToKel(right.Value),
                    TemperatureUnits.Fahrenheit => left.Value + right.FarToKel(right.Value),
                    TemperatureUnits.Kelvin => left.Value + right.Value,
                    _ => left.Value
                },
                _ => left.Value,
            };
            left.Value = amount;
            return left;
        }

        public static Temperature operator -(Temperature left, Temperature right)
        {
            double amount = left.Unit switch
            {
                TemperatureUnits.Celsius => right.Unit switch
                {
                    TemperatureUnits.Celsius => left.Value - right.Value,
                    TemperatureUnits.Fahrenheit => left.Value - right.FarToCel(right.Value),
                    TemperatureUnits.Kelvin => left.Value - right.KelToCel(right.Value),
                    _ => left.Value
                },
                TemperatureUnits.Fahrenheit => right.Unit switch
                {
                    TemperatureUnits.Celsius => left.Value - right.CelToFar(right.Value),
                    TemperatureUnits.Fahrenheit => left.Value - right.Value,
                    TemperatureUnits.Kelvin => left.Value - right.KelToFar(right.Value),
                    _ => left.Value
                },
                TemperatureUnits.Kelvin => right.Unit switch
                {
                    TemperatureUnits.Celsius => left.Value - right.CelToKel(right.Value),
                    TemperatureUnits.Fahrenheit => left.Value - right.FarToKel(right.Value),
                    TemperatureUnits.Kelvin => left.Value - right.Value,
                    _ => left.Value
                },
                _ => left.Value,
            };
            left.Value = amount;
            return left;
        }
        #endregion
    }
}
